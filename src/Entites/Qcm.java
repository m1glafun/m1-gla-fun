package Entites;

import java.util.ArrayList;
import java.util.List;

public class Qcm extends Exercice{

	private List<Entitie> listReponsesQcm;
	
	
	public Qcm(int idQcm, int idfeuille,int numExo, Question question){
		super(idQcm,idfeuille, numExo, question );
		this.listReponsesQcm = new ArrayList<Entitie>();
	}
	
	public Qcm(Qcm copy){
		super(copy);
		setReponses(copy.getReponses());
	}
	
	public Qcm(int idQcm, int idfeuille,int numExo, Question question, List<Entitie> list) {
		super(idQcm,idfeuille, numExo, question );
		this.listReponsesQcm = list;
	}

	public List<Entitie> getReponses(){
		return this.listReponsesQcm;
	}
	public void setReponses(List<Entitie> reponsesQcm) {
		if (reponsesQcm == null || reponsesQcm.isEmpty())
			throw new InvalidDevoirException(" liste de reponses vide", 61);
		setReponses(this.getReponses());
	}
	
	public void addReponse(ReponseQcm reponse){
		this.listReponsesQcm.add(reponse);
	}
/*	
	@Override
	public String toString(){
		
		for (int i=0; i < this.listReponsesQcm.size(); i++){
	//		 String s = s.concat( listReponsesQcm.get(i).getChaine());
		}
		sreturn "["+"" +"]";
	}*/
}
