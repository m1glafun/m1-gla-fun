package Entites;

public class Question extends Entitie{
	
	public Question(int id, String quest){
		this.setId(id);
		setChaine(quest);
	}
		
	//constructeur pour initiation
	public Question(int idQuestion, int numQuestion, String enoncee, float baremeQuest){
		super(idQuestion, numQuestion, enoncee,baremeQuest);
	}
	
	public Question(Question copy){
			super(copy);
	}
	
	@Override
	public void setNote(float baremeQuest){
		if (baremeQuest <= 0 )
			throw new InvalidDevoirException("invalid note ["+ baremeQuest+"]", 62);
		super.setNote(baremeQuest);
	}
	
	@Override
	public void setChaine(String request){
		if (request == null || request.trim().length() == 0)
			throw new InvalidDevoirException("invalid request["+request+" ]",63);
		super.setChaine( request);
	}
	

}
