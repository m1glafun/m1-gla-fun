package Entites;

public class Entitie {
	private int id;
	private String chaine;
	private float note;
	private int numero;	
	//private boolean isSelect;
	
	public Entitie(){}
	
	public Entitie(int id, int numero){
		this.id = id;
		this.numero = numero;
	}
	
	public Entitie(int id, int numero, String chaine){
		this.id = id;
		this.numero = numero;
		this.chaine = chaine;
	}
	public Entitie(int id, int numero, String chaine, float note){
		this.id = id;
		this.numero = numero;
		this.chaine = chaine;
		this.note = note;	
		//this.isSelect = false;
	}
	
	public Entitie(Entitie copy){
		setId(copy.getId());
		setChaine(copy.getChaine());
		setNote(copy.getNote());
	}
	
	public void setId(int id) {
		if (id < 0)
		throw new InvalidDevoirException("invalid id["+id+" ]",31);
		
	}

	public int getId() {
		return this.id;
	}

	public void setChaine(String chaine) {
		this.chaine = chaine;
	}

	public String getChaine() {
		return this.chaine;
	}

	public void setNote(float note) {
		this.note = note;		
	}

	public float getNote() {
		return this.note;
	}	
}
