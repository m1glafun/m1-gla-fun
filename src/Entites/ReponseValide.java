package Entites;

import java.text.SimpleDateFormat;

public class ReponseValide extends Entitie{
	private int idQuestion;
	private int idReponsevalide;
	private boolean valide;
	int numeroReponse;
	ReponseQcm reponseValide;
	//liste reponses valides pour QCM � faire
	
	public ReponseValide(int question,int idReponseValide, int numeroReponse){
		super(idReponseValide, numeroReponse);
		this.idQuestion = question;
		this.valide = true;
	}
	
	public ReponseValide (int question,int idReponseValide, int numero, String solution ){
		super(idReponseValide, numero, solution );
		this.idQuestion = question;
		this.valide = true;
	}	
	
	public ReponseValide(ReponseValide copy){
		super(copy);
		setValide(copy.getValide());		
	}
	
	
	public boolean getValide(){
		return valide;
	}
	
	public void setValide(boolean isValide){
		if (isValide == false)
			throw new InvalidDevoirException("ce n'est pas une solution valide",51);
			valide = isValide;
	}

	@Override
	public String toString(){
		return "["+this.idQuestion+this.idReponsevalide+this.numeroReponse+"]";//+reponseValide.getChaine()+"]";
	}
}
