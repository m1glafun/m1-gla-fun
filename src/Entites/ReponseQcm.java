package Entites;

public class ReponseQcm extends Entitie{
	private int idQuestion;
	private boolean isSelect;
	private int idReponse;
	private int numeroReponse;
	
	public ReponseQcm(String reponse){
		this.setId(idQuestion);
		this.setChaine(reponse);
	}
	public ReponseQcm(int idQuestion, String reponse){
		this.setId(idQuestion);
		this.setChaine(reponse);
		this.isSelect = false;
	}
	
	public ReponseQcm(int idReponse, int numberReponse, String solution, float note, int idQuestion){
		super(idReponse,numberReponse, solution, note);
		this.idQuestion = idQuestion;
		this.isSelect = false;
	}
	public ReponseQcm(int idReponse, int numberReponse, String solution, float note, int idQuestion, boolean isSelect){
		super(idReponse,numberReponse, solution, note);
		this.idQuestion = idQuestion;
		setIsSelect(isSelect);
	}
	//construsteur par copy
	public ReponseQcm(ReponseQcm copy){
		super(copy);
		setIsSelect(copy.getIsSelect());
	}

	public boolean getIsSelect(){
		return this.isSelect;
	}
	public void setIsSelect(boolean isSelect){
		this.isSelect = isSelect;
	}
	
	@Override
	public String toString(){
		return "["+this.idQuestion+this.idReponse+this.numeroReponse+this.getChaine()+"]";
	}
	
}
