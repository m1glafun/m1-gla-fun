package Entites;

import java.util.ArrayList;
import java.util.Date;

public class FeuilleDevoir {
	private int id;
	private String nomFeuille;
	private int idCours;
	private int idPartie;
	private int idChapitre;
	private int idProf;
	private Date dateCreated;
	private Date dateForSoumettre;
	private EtatCorrection etatCorrection;
	private boolean isRendu;
	
	//constructeur pour l'examen final
	public FeuilleDevoir(int id, String name, int idProf, int idCours,
			Date dateCreated, Date dateForSoumettre )
	{
		this.idCours = id;
		this.nomFeuille = name;
		this.idProf = idProf;
		this.idCours = idCours;
		this.dateCreated= dateCreated;
		this.dateForSoumettre = dateForSoumettre;
		this.etatCorrection = EtatCorrection.NC;
		this.isRendu = false;
	}
	
	//constructeur pour l'excercice de partie
		public FeuilleDevoir(int id, String name, int idProf, int idCours, int idPartie,
				Date dateCreated, Date dateForSoumettre )
		{
			this.idCours = id;
			this.nomFeuille = name;
			this.idProf = idProf;
			this.idCours = idCours;
			this.idPartie = idPartie;
			this.dateCreated = dateCreated;
			this.dateForSoumettre = dateForSoumettre;
			this.etatCorrection = EtatCorrection.NC;
			this.isRendu = false;
		}
		
		//constructeur pour l'excercice de chapitre
		public FeuilleDevoir(int id, String name, int idProf, int idCours, int idPartie,
				int idChapitre, Date dateCreated, Date dateForSoumettre )
		{
			this.idCours = id;
			this.nomFeuille = name;
			this.idProf = idProf;
			this.idCours = idCours;
			this.idPartie = idPartie;
			this.idChapitre = idChapitre;
			this.dateCreated = dateCreated;
			this.dateForSoumettre = dateForSoumettre;
			this.etatCorrection = EtatCorrection.NC;
			this.isRendu = false;
		}
	
	//constructeur par copie
	public FeuilleDevoir(FeuilleDevoir copyDevoir){
		if (copyDevoir == null)
			throw new InvalidUserException("invalid devoir",10);
		setIdFeuille(copyDevoir.getIdFeuille());
		setNomFeuille(copyDevoir.getNomFeuille());
		setIdProf(copyDevoir.getIdProf());
		setIdCours(copyDevoir.getIdCours());
		setIdPartie(copyDevoir.getIdPartie());
		setIdChapitre(copyDevoir.getIdChapitre());
		setDateCreated(copyDevoir.getDateCreated());
		setDateForSoumettre(copyDevoir.getDateForSoumettre());
		setEtatCorrection(copyDevoir.getEtatCorrection());
		setIsRendu(copyDevoir.getIsRendu());
	}
	
	//get-set
	
	public void setIdFeuille(int idFeuille) {
		if (idFeuille < 0)
			throw new InvalidDevoirException("invalide id cours ["+idFeuille+" ]",1);
		this.id = idFeuille;
	}

	public int getIdFeuille() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
	public void setNomFeuille(String titre) {
		if (titre == null || titre.trim().length() == 0)
			throw new InvalidDevoirException("invalid cours titre["+titre+" ]",9);
		this.nomFeuille = titre;
	}

	public String getNomFeuille() {
		return this.nomFeuille;
	}
	
	public void setIdProf(int idProf) {
			if (idProf < 0)
				throw new InvalidDevoirException("invalide id cours ["+idProf+" ]",8);
			this.idProf = idProf;
		}

	public int getIdProf() {
			return idProf;
		}

	public void setIdCours(int idCours) {
			if (idCours < 0)
				throw new InvalidDevoirException("invalide id cours ["+idCours+" ]",7);
			this.idCours = idCours;
		}

	public int getIdCours() {
			return idCours;
		}
	
	public void setEtatCorrection(EtatCorrection etatCorrection) {
		if (etatCorrection != EtatCorrection.C || etatCorrection != EtatCorrection.NC 
				|| etatCorrection != EtatCorrection.EC)
			throw new InvalidDevoirException("seuls les etats valides: [EC,NC,C]",6);
		this.etatCorrection = etatCorrection;
	}

	public EtatCorrection getEtatCorrection() {
		return this.etatCorrection;
	}

	public void setDateForSoumettre(Date dateForSoumettre) {
		if (dateForSoumettre == null)
			throw new InvalidDevoirException("invalid date soumission",3);
		this.dateForSoumettre = dateForSoumettre;
	}

	public Date getDateForSoumettre() {
		return this.dateForSoumettre;
	}

	public void setDateCreated(Date dateCreated) {
		if (dateCreated == null)
			throw new InvalidDevoirException("invalid date creation",2);
		this.dateCreated = dateCreated;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setIdChapitre(int idChapitre) {
		if (idChapitre < 0)
			throw new InvalidDevoirException("invalide id cours ["+idChapitre+" ]",5);
		this.idChapitre = idChapitre;
	}

	public int getIdChapitre() {
		return this.idChapitre;
	}

	public void setIdPartie(int idPartie) {
		if (idPartie < 0)
			throw new InvalidDevoirException("invalide id cours ["+idPartie+" ]",4);
		this.idPartie = idPartie;
	}

	public int getIdPartie() {
		// TODO Auto-generated method stub
		return this.idPartie;
	}
	
	public void setIsRendu(boolean rendu) {
		this.isRendu = rendu;
	}

	public boolean getIsRendu() {
		return this.isRendu;
	}

}
