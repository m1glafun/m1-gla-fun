package Entites;

import java.util.ArrayList;
import java.util.List;

public class Exercice
{
	private int idExo;
	private int idFeuille;	
	private int numberExo;
	private Question question;
	//private List<Question> listReponses;
	private boolean isFait;
	
	
	public Exercice(){}
	
	public Exercice(int id, int idFeuille, int numExo, Question question ){
		this.idExo = id;
		this.idFeuille = idFeuille;
		this.numberExo = numExo;
		this.question = question;
	//	this.listReponses = new ArrayList<Entitie>();
	}
	
	public Exercice(int id, int idFeuille, int numExo, Entitie question,List<Entitie> listReponses ){
		setIdExo(id);
		setNumberExo(numExo);
		setIdFeuille(idFeuille);
		setNumberExo(numExo);
		//setReponses(listReponses);
	}
	
	public Exercice(Exercice copy){
		setIdExo(copy.getIdExo());
		setIdFeuille(getIdFeuille());
		setNumberExo(copy.getNumberExo());
		setIdFeuille(copy.getIdFeuille());
		setNumberExo(copy.getNumberExo());
		//setReponses(copy.getReponses());
	}
	
	public void setIdExo(int idExo) {
		this.idExo = idExo;
	}

	public int getIdExo() {
		return idExo;
	}
/*
	public void setReponses(List<Entitie> reponses) {
		this.listReponses = reponses;
	}

	public List<Entitie> getReponses() {
		return this.listReponses;
	}
*/
	public void setIdFeuille(int idFeuille) {
		if (idFeuille < 0)
			throw new InvalidDevoirException("invalid idQuestion["+idFeuille+" ]",21);
		
	}

	public int getIdFeuille() {
		return this.idFeuille;
	}

	public void setNumberExo(int numberExo) {
		if (numberExo < 0)
			throw new InvalidDevoirException("invalid idQuestion["+numberExo+" ]",22);
		this.numberExo = numberExo;
	}

	public int getNumberExo() {
		return this.numberExo;
	}

	

	/* la note d'un excercice est la somme des notes de questions
	public float noteExercice(Qcm exo){
		float som = 0;
		for (int i=0; i < this.reponses.size(); i++)
			som = som + reponses.get(i).getNote();
		noteFinal = som;
		return noteFinal;
	} */
}
