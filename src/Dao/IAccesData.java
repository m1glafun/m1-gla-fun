package Dao;

import java.util.List;

import Entites.FeuilleDevoir;
import Entites.FeuilleSoumise;
//import Entites.FeuilleSoumission;
import Entites.User;

public interface IAccesData {
	//liste des user
	public List<User> getAllUser();
	
	//liste des feuilles d'excercices
	public List<FeuilleDevoir> getAllFeuilleExo();
	
	//liste des feuilles d�j� soumises
	public List<FeuilleSoumise> getAllCours();
	
	
	//liste des excerices d'un professeur
	public List<FeuilleDevoir> getCoursProf(int idProf);
	
	//ajouter un cours
	public int ajoutCours(int idProf, String titreCours);
	
	//ajouter une FeuilleExo 
	public int ajourtFeuilleExo(int idProf, String nameFeuille);
	
	//liste des Feuilles de Soumission d'un etudiant d�ja soumettre
//	public List<FeuilleSoumission> getAllFeuilleSoumission(int idEtudiant);
	
}
