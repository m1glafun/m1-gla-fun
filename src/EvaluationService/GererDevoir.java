package EvaluationService;

import Entites.Exercice;

public abstract class GererDevoir {
	Exercice exo;

	public GererDevoir(){}
	
	public GererDevoir(Exercice ex){
		setExercice(ex);
	}
	
	
	
	//choisir un excercice � corriger
	public void setExercice(Exercice exo) {
		this.exo = exo;
	}

	public abstract void corriger(Exercice exo);
}
