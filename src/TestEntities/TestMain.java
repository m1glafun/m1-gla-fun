package TestEntities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import Entites.*;

public class TestMain {
	public static void main(String[] args){
		User user=new Etudiant(1, "Anthony","inconnu :D",
				new Date("11/04/2014"),"lethanh11_4@hotmail.com");
		System.out.println(user.toString());
		Professeur pro = new Professeur(2, "le","thanh",
				new Date("11/04/2014"),"lethanh11_4@hotmail.com");
		System.out.println(pro.toString());
		System.out.println("***********************");
		
		//liste de reponses Qcm
		List<Entitie> listReponses = new ArrayList<Entitie>();
		
		//creer Reponse(idQuestion, reponse);
		ReponseQcm reponse = new ReponseQcm(1, "som egale � 2");
		listReponses.add(0, reponse);
		listReponses.add(1, new ReponseQcm(1, "som egale � 3"));
		listReponses.add(2, new ReponseQcm(1,"som est 5"));
		
		//creer question Qcm
		Exercice qcm1 = new Qcm(1,1,1, new Question(1, "1 + 1 egale combien?"),listReponses);
		
		
		//creer QCM corrig� : c'est la question 1 de la question 1
		ReponseValide reponseValide = new ReponseValide(1,1, 1); //reponse.getChaine() );
		
		System.out.println(reponseValide.toString());
		
		System.out.println("***********************");
		
		
	}
	
	
}
